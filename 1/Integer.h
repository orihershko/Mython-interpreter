#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
public:
	Integer(int var, bool isTemp);
	~Integer();
	bool isPrintable();
	std::string ToString() const;
	std::string getType();
	Type* operator=(const Type& other);
private:
	int _var;
};



#endif // INTEGER_H