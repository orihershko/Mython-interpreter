#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type
{
public:
	Void(bool isTemp);
	~Void();
	bool isPrintable();
	std::string ToString() const;
	std::string getType();
	Type* operator=(const Type& other);
private:
};



#endif // VOID_H