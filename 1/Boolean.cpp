#include "Boolean.h"

Boolean::Boolean(bool var , bool isTemp) : Type(isTemp)
{
	_var = var;
}

Boolean::~Boolean()
{
}

bool Boolean::isPrintable()
{
	return true;
}

std::string Boolean::ToString() const
{
	std::string str;
	if (_var)
	{
		str = "True";
	}
	else
	{
		str = "False";
	}
	return str;
}

std::string Boolean::getType()
{
	return "Boolean";
}

Type * Boolean::operator=(const Type & other)
{
	bool val;
	if (other.ToString() == "True")
	{
		val = true;
	}
	else
	{
		val = false;
	}
	_var = val;
	return this;
	
}
