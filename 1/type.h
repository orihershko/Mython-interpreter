#ifndef TYPE_H
#define TYPE_H

#include<string>
class Type
{
protected:
	bool _isTemp;
public:
	Type(bool isTemp);
	~Type();
	bool getIsTemp() const;
	void setIsTemp(bool isTemp);
	virtual bool isPrintable() = 0;
	virtual std::string ToString() const = 0 ;
	virtual std::string getType() = 0;
	virtual Type* operator=(const Type& other) = 0;
};




#endif //TYPE_H
