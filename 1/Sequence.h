#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "type.h"

class Sequence : public Type
{
public:
	Sequence(bool isTemp);
	~Sequence();
	bool isPrintable();
	virtual std::string ToString() const = 0 ;
	virtual std::string getType() = 0;
	virtual Type* operator=(const Type& other) = 0;

};


#endif // SEQUENCE_H