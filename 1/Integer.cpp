#include "Integer.h"

Integer::Integer(int var, bool isTemp) : Type(isTemp)
{
	_var = var;
}

Integer::~Integer()
{
}

bool Integer::isPrintable()
{
	return true;
}

std::string Integer::ToString() const
{

	return std::to_string(_var);
}

std::string Integer::getType()
{
	return "Integer";
}

Type * Integer::operator=(const Type & other)
{
	int num = std::atoi(other.ToString().c_str());
	_var = num;
	return this;
}
