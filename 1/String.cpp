#include "String.h"
#define QUOT char(39)
#define DOUBLE_QOUTES char(34)
String::String(std::string var, bool isTemp)  : Sequence(isTemp)
{
	_var = var;
}

String::~String()
{
}

std::string String::ToString() const
{
	std::string str = _var.substr(1,(_var.size()-2)); // without quot
	// 39 in ascii is ', 34 in ascii is "
	std::size_t index = str.find_first_of(QUOT);
	while (index != std::string::npos)
	{
		str[index] = DOUBLE_QOUTES;
		index = str.find_first_of(QUOT, index + 1);
	}
	
	
	str = QUOT + str + QUOT;
	return str;
}

std::string String::getType()
{
	return "String";
}

Type * String::operator=(const Type & other)
{
	_var = other.ToString();
	return this;
}
