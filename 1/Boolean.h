#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type
{
public:
	Boolean(bool var, bool isTemp);
	~Boolean();
	bool isPrintable();
	std::string ToString() const;
	std::string getType();
	Type* operator=(const Type& other);
private:
	bool _var;

};

#endif // BOOLEAN_H