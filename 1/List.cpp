#include "List.h"
#include "parser.h"
#include "Helper.h"
#include<vector>
List::List(std::string str,bool isTemp) : Sequence(isTemp)
{
	str = str.substr(1, str.size()-2); // remove '[' and ']'
	std::size_t varP;
	std::string var;
	//bool nextStr;
	do
	{
		varP = str.find_first_of(",");
		/*if ((str[varP + 1] == 34 || str[varP - 1] == 39) && (str[varP + 1] == 34 || str[varP + 1] == 39))
		{
			varP = str.find(",", varP + 1);
		}*/
		var = str.substr(0, varP);
		str = str.substr(varP + 1, str.size());
		Helper::trim(var);
		Type * t = Parser::getType(var);
		append(t);
	} while (varP != std::string::npos);
	
}

List::~List()
{
}

void List::append(Type * t)
{
	_list.push_back(t);
}

void List::del(int index)
{
	_list.erase(_list.begin()+index);
}

std::string List::ToString() const
{
	std::string print = "[";
	for (int i = 0; i < _list.size(); i++)
	{
		if (i == _list.size() - 1)
		{
			print = print + _list[i]->ToString();
		}
		else
		{
			print = print + _list[i]->ToString() + ",";
		}
		
	}
	print = print + "]";
	return print;
}

std::string List::getType()
{
	return "List";
}

Type * List::operator=(const Type & other)
{
	List temp(other.ToString(), true);
	std::vector<Type*> tempList = getList();
	for (int i = 0; i < _list.size(); i++)
	{
		del(i);
	}
	for (int i = 0; i < tempList.size(); i++)
	{
		append(tempList[i]);
	}
	return this;

}

std::vector<Type*> List::getList()
{
	return _list;
}
