#include "Void.h"

Void::~Void() 
{
}

Void::Void(bool isTemp) : Type(isTemp)
{
}

bool Void::isPrintable()
{
	return false;
}

std::string Void::ToString() const
{
	return std::string();
}

std::string Void::getType()
{
	return "Void";
}

Type * Void::operator=(const Type & other)
{
	return this;
}
