#include "type.h"


Type::Type(bool isTemp)
{
	_isTemp = isTemp;
}

Type::~Type()
{
}

bool Type::getIsTemp() const
{
	return _isTemp;
}

void Type::setIsTemp(bool isTemp)
{
	_isTemp = isTemp;
}
