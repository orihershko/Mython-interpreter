#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include<map>

class Parser
{
private:
	
	
	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	//static int _s;
	static Type* getVariableValue(const std::string &str);
	static Type* arithmetic(const std::string& str);
	static std::string * typeFunc(const std::string& str);


public:
	static std::unordered_map<std::string, Type*> _vars;
	static Type* parseString(std::string str) throw();
	static Type* getType(std::string &str);
	


};

#endif //PARSER_H
