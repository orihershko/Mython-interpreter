#include "parser.h"
#include <iostream>
#include "IndentationException.h"
#include<string>
#include"Integer.h"
#include"String.h"
#include"Void.h"
#include"Boolean.h"
#include "List.h"
#include"SyntaxException.h"
#include "NameErrorException.h"
#include<exception>
std::unordered_map<std::string, Type*> Parser::_vars;
Type* Parser::parseString(std::string str) throw()
{
	Type * t = nullptr;
	if (str.length() > 0)
	{
		if (str[0] == ' ' || str[0] == '\t')
		{
			throw IndentationException();
		}
		t = arithmetic(str);
		std::string * type = typeFunc(str);
		if (type != nullptr)
		{
			std::cout << *type << std::endl;
		}
		else if (t == nullptr)
		{
			if (!makeAssignment(str))
			{
				t = getVariableValue(str);
				if (t == nullptr)
				{
					t = getType(str);
					std::cout << t->ToString() << std::endl;
				}
				else
				{
					std::cout << t->ToString() << std::endl;
				}
			}
		}
		else
		{
			std::cout << t->ToString() << std::endl;
		}
		
		
	}

	return t;
}
//geting the type string int bool or void
Type * Parser::getType(std::string & str)
{
	Type * t = nullptr;
	Helper::trim(str);
	if (str == "") // void
	{
		t = new Void(true);
	}
	else if (Helper::isInteger(str)) // int
	{
		int num = std::atoi(str.c_str());
		t = new Integer(num, true);
	}
	else if(Helper::isBoolean(str)) // bool
	{
		bool val;
		if (str == "True")
		{
			val = true;
		}
		else
		{
			val = false;
		}
		t = new Boolean(val,true);
	}
	else if(Helper::isString(str)) //string
	{
		t = new String(str, true);
	}
	else if (Helper::isList(str))
	{
		t = new List(str, true);
	}
	else
	{
		throw SyntaxException();
	}
	return t;
}

bool Parser::isLegalVarName(const std::string & str)
{
	std::string name = str;
	if (Helper::isDigit(name[0])) // first letter is digit
	{
		throw NameErrorException(name);
	}
	for (int i = 1; i < name.size(); i++)
	{
		if (!(Helper::isLetter(name[i]) || Helper::isUnderscore(name[i]) || Helper::isDigit(name[i])))
		{
			throw NameErrorException(name);;
		}
	}
	return true;
}

bool Parser::makeAssignment(const std::string & str)
{
	std::size_t pos = str.find("=");
	std::string temp = str;
	if (pos != std::string::npos)
	{
		//example: name = 89
		std::string name = str.substr(0, pos);
		Helper::trim(name);
		std::string var = str.substr(pos + 1, str.size());// delete the '='
		Helper::trim(var);


		if (!isLegalVarName(name))
		{
			return false;
		}
		Type * t = getVariableValue(var);
		if (t == nullptr)
		{
			t = arithmetic(var);
			if (t==nullptr)
			{
				t = getType(var);
			}
			
		}
		auto got = _vars.find(name);
		if (got == _vars.end())
		{
			_vars.insert(std::make_pair(name, t));
		}
		else
		{
			_vars.erase(got->first);
			_vars.insert(std::make_pair(name, t));
		}
		
		return true;
	}
	else if(Helper::isAppend(temp))
	{
		//example: list.append(90)
		std::size_t pos = str.find(".");
		std::string nameList = str.substr(0, pos);
		std::size_t startPos = str.find("(");
		std::size_t endPos = str.find(")");
		if (startPos == std::string::npos || endPos == std::string::npos)
		{
			return false;
		}
		if (!isLegalVarName(nameList))
		{
			return false;
		}

		std::string var = str.substr(startPos + 1, str.size() - 2);
		var.pop_back();
		Type * t = getVariableValue(var);
		if (t == nullptr)
		{
			t = getType(var);
		}
		auto got = _vars.find(nameList);
		if (got == _vars.end())
		{
			_vars.insert(std::make_pair(nameList, t));
		}
		else
		{ 
			//example: [1,2,"90"]
			std::string tempVar = got->second->ToString();
			delete t;
			t = nullptr;
			tempVar = tempVar.substr(0, tempVar.size() - 1);
			tempVar = tempVar + "," + var + "]";
			t = getType(tempVar);
			*(got->second) = *t;

			return true;
		}
		
	}
	
	return false;
}

Type * Parser::getVariableValue(const std::string & str)
{
	auto got = _vars.find(str);
	if (got == _vars.end())
	{
		return nullptr;
	}
	else
	{
		return got->second;
	}

	
}

Type * Parser::arithmetic(const std::string & str) 
{
	std::string type;
	type = "+";
	if (Helper::isArithmetic(str,type))
	{
		//example: x+3
		std::size_t pos = str.find(type);
		std::string val1 = str.substr(0, pos);
		std::string val2 = str.substr(pos + 1, str.size());
		Type * t = nullptr;
		Helper::trim(val1);
		Helper::trim(val2);
		Type * t1 = getVariableValue(val1);
		Type * t2 = getVariableValue(val2);
		int* sum = nullptr;
		if (Helper::isString(val1) && Helper::isString(val2)) // "asd"+"asad"
		{
			val1.erase(0,1);
			val1.erase(val1.size() - 1,1);
			val2.erase(0,1);
			val2.erase(val2.size() - 1,1);
			std::string allString = "\"" + val1 + val2 + "\"";
			t = getType(allString);
			return t;
		}
		else if (Helper::isInteger(val1) && Helper::isInteger(val2) ) // 5+6
		{
			sum = new int;
			*sum = std::atoi(val1.c_str()) + std::atoi(val2.c_str());
		
		}

		else if (Helper::isInteger(val1) && t2 != nullptr && t2->getType() == "Integer") // 3+x
		{
			sum = new int;
			*sum = std::atoi(val1.c_str()) + std::atoi(t2->ToString().c_str());
		
		}
		else if(Helper::isString(val1) && t2!= nullptr && t2->getType() == "String")
		{
			std::string strV2 = t2->ToString();
			strV2.erase(0, 1);
			strV2.erase(strV2.size() - 1, 1);
			val1.erase(0, 1);
			val1.erase(val1.size() - 1, 1);
			std::string allString = "\"" + val1 + strV2 + "\"";
			t = getType(allString);
			return t;
		}
		else if (Helper::isInteger(val2) && t1 != nullptr && t1->getType() == "Integer") // x+1
		{
			sum = new int;
			*sum = std::atoi(val2.c_str()) + std::atoi(t1->ToString().c_str());
		
		}
		else if(Helper::isString(val2) && t1 != nullptr && t1->getType() == "String")
		{
			std::string strV1 = t1->ToString();
			strV1.erase(0, 1);
			strV1.erase(strV1.size() - 1, 1);
			val2.erase(0, 1);
			val2.erase(val2.size() - 1, 1);
			std::string allString = "\"" + strV1+ val2 + "\"";
			t = getType(allString);
			return t;
		}
		else if (t1 != nullptr && t2 != nullptr) //x+y
		{
			if (t1->getType() == "Integer" && t2->getType() == "Integer")
			{
				sum = new int;
				*sum = std::atoi(t2->ToString().c_str()) + std::atoi(t1->ToString().c_str());
			}
			else if(t1->getType() == "String" && t2->getType() == "String")
			{
				std::string strV1 = t1->ToString();
				std::string strV2 = t2->ToString();
				strV1.erase(0, 1);
				strV1.erase(val1.size() - 1, 1);
				strV2.erase(0, 1);
				strV2.erase(val2.size() - 1, 1);
				std::string allString = "\"" + strV1 + strV2 + "\"";
				t = getType(allString);
				return t;
			}
			
		}
		if (sum != nullptr)
		{
			t = getType(std::to_string(*sum));
			return t;
		}
	

		

	}
	type = "/";
	if (Helper::isArithmetic(str, type)) //for now it work for INT
	{
		//example: x/3
		std::size_t pos = str.find(type);
		std::string val1 = str.substr(0, pos);
		std::string val2 = str.substr(pos + 1, str.size());
		Type * t = nullptr;
		Helper::trim(val1);
		Helper::trim(val2);
		Type * t1 = getVariableValue(val1);
		Type * t2 = getVariableValue(val2);
		int* sum =nullptr;
		if (Helper::isInteger(val1) && Helper::isInteger(val2)) // 5/6
		{
			
			if (std::atoi(val2.c_str()) != 0)
			{
				sum = new int;
				*sum = std::atoi(val1.c_str()) / std::atoi(val2.c_str());  
			}
			

		}

		if (Helper::isInteger(val1) && t2 != nullptr && t2->getType() == "Integer") // 3/x
		{
			
			if (std::atoi(t2->ToString().c_str()) != 0)
			{
				sum = new int;
				*sum = std::atoi(val1.c_str()) / std::atoi(t2->ToString().c_str());
			}
			

		}
		if (Helper::isInteger(val2) && t1 != nullptr && t1->getType() == "Integer") // x/1
		{
			
			if (std::atoi(val2.c_str()) != 0)
			{
				sum = new int;
				*sum = std::atoi(t1->ToString().c_str()) / std::atoi(val2.c_str());
			}
			
		}
		if (t1 != nullptr && t2 != nullptr && t2->getType() == "Integer" && t1->getType() == "Integer") //x/y
		{
			
			if (std::atoi(t2->ToString().c_str()) != 0)
			{
				sum = new int;
				*sum = std::atoi(t1->ToString().c_str()) / std::atoi(t2->ToString().c_str());
			}

		}
		if (sum != nullptr)
		{
			t = getType(std::to_string(*sum));
			return t;
		}
		
		
		
		
	}
	type = "*";
	if (Helper::isArithmetic(str, type))
	{
		//example: x*3
		std::size_t pos = str.find(type);
		std::string val1 = str.substr(0, pos);
		std::string val2 = str.substr(pos + 1, str.size());
		Type * t = nullptr;
		Helper::trim(val1);
		Helper::trim(val2);
		Type * t1 = getVariableValue(val1);
		Type * t2 = getVariableValue(val2);
		int* sum =nullptr;
		if (Helper::isInteger(val1) && Helper::isInteger(val2)) // 5*6
		{
			sum = new int;
			*sum = std::atoi(val1.c_str()) * std::atoi(val2.c_str());

		}

		if (Helper::isInteger(val1) && t2 != nullptr && t2->getType() == "Integer" )// 3*x
		{
			sum = new int;
			*sum = std::atoi(val1.c_str()) * std::atoi(t2->ToString().c_str());

		}
		if (Helper::isInteger(val2) && t1 != nullptr && t1->getType() == "Integer") // x*1
		{
			sum = new int;
			*sum = std::atoi(val2.c_str()) * std::atoi(t1->ToString().c_str());

		}
		if (t1 != nullptr && t2 != nullptr && t2->getType() == "Integer" && t1->getType() == "Integer") //x*y
		{
			sum = new int;
			*sum = std::atoi(t2->ToString().c_str()) * std::atoi(t1->ToString().c_str());

		}
		if (sum != nullptr)
		{
			t = getType(std::to_string(*sum));
			return t;
		}
		

	}
	type = "-";
	if (Helper::isArithmetic(str, type))
	{
		//example: x-3
		std::size_t pos = str.find(type);
		std::string val1 = str.substr(0, pos);
		std::string val2 = str.substr(pos + 1, str.size());
		Type * t = nullptr;
		Helper::trim(val1);
		Helper::trim(val2);
		Type * t1 = getVariableValue(val1);
		Type * t2 = getVariableValue(val2);
		int* sum = nullptr;
		if (Helper::isInteger(val1) && Helper::isInteger(val2)) // 5-6
		{
			sum = new int;
			*sum = std::atoi(val1.c_str()) - std::atoi(val2.c_str());

		}

		if (Helper::isInteger(val1) && t2 != nullptr && t2->getType() == "Integer") // 3-x
		{
			sum = new int;
			*sum = std::atoi(val1.c_str()) - std::atoi(t2->ToString().c_str());

		}
		if (Helper::isInteger(val2) && t1 != nullptr && t1->getType() == "Integer") // x-1
		{
			sum = new int;
			*sum = std::atoi(val2.c_str()) - std::atoi(t1->ToString().c_str());

		}
		if (t1 != nullptr && t2 != nullptr && t2->getType() == "Integer" && t1->getType() == "Integer") //x-y
		{
			sum = new int;
			*sum = std::atoi(t1->ToString().c_str()) - std::atoi(t2->ToString().c_str());

		}
		if (sum != nullptr)
		{
			t = getType(std::to_string(*sum));
			return t;
		}
		

	}
	return nullptr;
	
}

std::string * Parser::typeFunc(const std::string & str)
{
	std::string temp = str;
	Helper::trim(temp);
	std::size_t start = temp.find("type(");
	if (start != std::string::npos)
	{
		std::string val = temp.substr(start + 5,temp.size());
		std::size_t end = val.find_last_of(")");
		if (end != std::string::npos)
		{
			val.erase(end);
			
			try
			{
				Type * t = getType(val);
				std::string* k = new std::string(t->getType());
				return k;
			}
			catch (const InterperterException& e)
			{
				Type *t = getVariableValue(val);
				if (t != nullptr)
				{
					std::string* k = new std::string(t->getType());
					return k;
				}
			}
		}
	}
	return nullptr;
	
	

}


