#ifndef LIST_H
#define LIST_H
#include"Sequence.h"
#include<string>
#include"type.h"
#include<vector>
class List : public Sequence
{
public:
	List(std::string str, bool isTemp);
	~List();
	void append(Type * t);
	void del(int index);
	std::string ToString() const;
	std::string getType();
	Type* operator=(const Type& other);
	std::vector<Type*> getList();
private:
	std::vector<Type*> _list;
};

#endif // LIST_H