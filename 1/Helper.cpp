#include "Helper.h"

bool Helper::isInteger(const std::string& s)
{
	int start = (s[0] == '-') ? 1 : 0;
	for (int i = start; i < s.size(); i++)
	{
		if (!isDigit(s[i]))
		{
			return false;
		}
	}

	return true;
}

bool Helper::isBoolean(const std::string& s)
{
	return (s == "True" || s == "False");
}


bool Helper::isString(const std::string& s)
{
	size_t end = s.size() - 1;

	if (s[0] == '\"' && s[end] == '\"')
	{
		return true;
	}
	if (s[0] == '\'' && s[end] == '\'')
	{
		return true;
	}

	return false;

}

bool Helper::isList(const std::string & s)
{
	if (s[0] == '[' && s[s.size()-1] == ']')
	{
		return true;
	}
	return false;
}

bool Helper::isDigit(char c)
{
	return (c >= '0' && c <= '9');
}

void Helper::trim(std::string &str)
{
	rtrim(str);
	ltrim(str);

}

void Helper::rtrim(std::string &str)
{
	size_t endpos = str.find_last_not_of(" \t");
	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
	}
}

void Helper::ltrim(std::string &str)
{
	size_t startpos = str.find_first_not_of(" \t");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

void Helper::removeLeadingZeros(std::string &str)
{
	size_t startpos = str.find_first_not_of("0");
	if (std::string::npos != startpos)
	{
		str = str.substr(startpos);
	}
}

bool Helper::isAppend(std::string & str)
{
	trim(str);
	std::size_t pos = str.find(".append(");
	if (pos == std::string::npos || pos == 0)
	{
		return false;
	}
	else
	{
		if (str[pos - 1] == ' ')
		{
			return false;
		}
		return true;
	}
}

bool Helper::isArithmetic(const std::string & str, const std::string operatin)
{
	std::size_t i = str.find(operatin);
	if (i == std::string::npos)
	{
		return false;
	}
	else
	{
		std::size_t temp = (str.substr(i + 1, str.size())).find(operatin);
		if (temp != std::string::npos)
		{
			return false;
		}
	}
	return true;
}






bool Helper::isLowerLetter(char c)
{
	return (c >= 'a' && c <= 'z');
}

bool Helper::isUpperLetter(char c)
{
	return (c >= 'A' && c <= 'Z');
}

bool Helper::isLetter(char c)
{
	return (isLowerLetter(c) || isUpperLetter(c));
}

bool Helper::isUnderscore(char c)
{
	return ('_' == c);
}

