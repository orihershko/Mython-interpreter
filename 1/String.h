#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
public:
	String(std::string var, bool isTemp);
	~String();
	std::string ToString() const;
	std::string getType();
	Type* operator=(const Type& other);
private:
	std::string _var;
};

#endif // STRING_H