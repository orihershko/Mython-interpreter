#include "NameErrorException.h"
#include <cstring>
#include<string>
#include<iostream>
NameErrorException::NameErrorException(std::string str)
{
	_name = str;
}

const char * NameErrorException::what() const throw()
{
	std::string * str = new std::string("NameError: name \'" + _name + "\' is not defined");
	/*char *temp = new char[str.length() + 1];
	strcpy(temp, str.c_str());*/
	return str->c_str();
}
